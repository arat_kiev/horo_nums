<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

class CartInfo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if (Auth::user()) {
         $user = Auth::user();
        } else {
          $user = NULL;
        }
        return view("widgets.cart_info", [
            'config' => $this->config,
            'cart_items' => Cart::content(),
            'user' => $user
        ]);
    }
}