<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Payments\WayForPay;

class Payment extends Model
{
    protected $fillable = ['name', 'merchant', 'private_key', 'public_key', 'enabled'];
}
