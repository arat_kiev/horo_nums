<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = [
      'user_id',
      'status_id',
      'invoice',
      'type',
      'total',
      'payed',
      'user_comments',
      'updated_at'
  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function status()
  {
    return $this->belongsTo('App\OrderStatus');
  }

  public function products() {
    return $this->hasMany('App\OrderProduct');
  }
}
