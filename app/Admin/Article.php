<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(App\Config::class, function (ModelConfiguration $model) {
  $model->setTitle('Общие настройки');
  // Создание записи
  $model->setMessageOnCreate('Общие настройки добавлены');
  // Редактирование записи
  $model->setMessageOnUpdate('Общие настройки обновлены');
  // Удаление записи
  $model->setMessageOnDelete('Общие настройки удалены');
  // Восстановление записи
  $model->setMessageOnRestore('Общие настройки восстановлены');
  // Display
  $model->onDisplay(function () {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Название')->required()->unique(),
        AdminFormElement::text('merchant', 'Мерчант')->required(),
        AdminFormElement::text('private_key', 'Публичный ключ')->required(),
        AdminFormElement::text('public_key', 'Приватный ключ')->required(),
        AdminFormElement::checkbox('enabled', 'Включить')
    );
    return $form;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Название')->required()->unique(),
        AdminFormElement::text('merchant', 'Мерчант')->required(),
        AdminFormElement::text('private_key', 'Публичный ключ')->required(),
        AdminFormElement::text('public_key', 'Приватный ключ')->required(),
        AdminFormElement::checkbox('enabled', 'Включить')
    );
    return $form;
  });
});