<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(App\Page::class, function (ModelConfiguration $model) {
  $model->setTitle('Страницы сайта');
  // Создание записи
  $model->setMessageOnCreate('Страница создана');
  // Редактирование записи
  $model->setMessageOnUpdate('Страница обновлена');
  // Удаление записи
  $model->setMessageOnDelete('Страница удалена');
  // Восстановление записи
  $model->setMessageOnRestore('Страница восстановлена');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatablesAsync()->setColumns([
        AdminColumn::text('id')->setLabel('#'),
        AdminColumn::link('name')->setLabel('Заголовок'),
        AdminColumn::text('alias')->setLabel('Ссылка'),
        AdminColumn::custom('Блокировка', function(Model $model) {
          $label = $model->blocked == true ? '<span class="label label-danger">Заблокирована</span>' : '<span class="label label-success">Разблокирована</span>';
          return $label;
        }),
        AdminColumn::custom('Статус', function(Model $model) {
          $label = $model->enabled == true ? '<span class="label label-success">Опубликовано</span>' : '<span class="label label-danger">Не опубликовано</span>';
          return $label;
        }),
        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
    ]);
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Заголовок')->required()->unique(),
        AdminFormElement::text('meta_title', 'Мета заголовок (SEO)'),
        AdminFormElement::textarea('meta_description', 'Мета описание (SEO)'),
        AdminFormElement::text('alias', 'Ссылка (алиас)'),
        AdminFormElement::text('sort', 'Порядок сортировки'),
        AdminFormElement::ckeditor('body', 'Наполнение страницы')->required(),
        AdminFormElement::checkbox('blocked', 'Блокировка'),
        AdminFormElement::checkbox('enabled', 'Опубликовать'),
        AdminFormElement::custom()->setCallback(function($instance) {
          if (empty($instance->alias)) {
            $instance->alias = Slug::convert($instance->name, '-');
          }
        })
    );
    return $form;
  });
}) ->addMenuPage(App\Page::class, 0)
    ->setIcon('fa fa-file-text-o');