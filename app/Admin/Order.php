<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;
use App\ServiceType;
use App\OrderStatus;
use App\User;

AdminSection::registerModel(App\Order::class, function (ModelConfiguration $model) {
  $model->setTitle('Заказы');
  // Создание записи
  $model->setMessageOnCreate('Заказ создан');
  // Редактирование записи
  $model->setMessageOnUpdate('Заказ обновлен');
  // Удаление записи
  $model->setMessageOnDelete('Заказ удален');
  // Восстановление записи
  $model->setMessageOnRestore('Заказ восстановлен');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatables()->setColumns([
        AdminColumn::text('id')->setLabel('#'),
        AdminColumn::text('user.name')->setLabel('Имя'),
        AdminColumn::text('user.last_name')->setLabel('Фамилия'),
        AdminColumn::text('user.email')->setLabel('Email'),
        AdminColumn::text('invoice')->setLabel('Счет'),
//        AdminColumn::text('user.phone')->setLabel('Телефон'),
        AdminColumn::text('status.name')->setLabel('Статус'),
        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
    ]);
    $display->with('user');
    $display->with('status');

    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::custom()->setDisplay(function($instance) {
          return $instance->name;
        }),
        AdminFormElement::custom()->setDisplay(function($instance) {
          if (!empty($instance->user)) {
            return AdminFormElement::view('admin.orders.user_info', ['user' => $instance->user]);
          } else {
            return AdminFormElement::view('admin.orders.new_user');
          }
        }),
        AdminFormElement::custom()->setDisplay(function($instance) {
          if (!empty($instance->user)) {
            return AdminFormElement::view('admin.orders.order_header', ['products' => $instance->products]);
          }
        }),
        AdminFormElement::text('invoice', 'Счет'),
        AdminFormElement::select('status_id', 'Статус заказа')->setOptions(OrderStatus::statuses())->required(),
        AdminFormElement::select('type', 'Тип заказа')->setOptions(['horo' => 'Астрология', 'nums' => 'Нумерология'])->required(),
        AdminFormElement::file('result', 'Результат')->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
          return 'orders';
        }),
        AdminFormElement::text('total', 'Стоимость заказа (грн.)'),
        AdminFormElement::textarea('user_comments', 'Комментарии клиента'),
        AdminFormElement::textarea('admin_comments', 'Комментарии менеджера'),
        AdminFormElement::checkbox('payed', 'Оплачен'),
        AdminFormElement::custom()->setCallback(function($instance) {
          $request = \Request::all();

          //Обработка и создание нового клиента
          if (!empty($request['user_email'])) {
            if (!$user = User::where('email', $request['user_email'])->first()) {
              $user = User::create([
                  'name' => $request['user_name'],
                  'last_name' => $request['user_last_name'],
                  'middle_name' => $request['user_middle_name'],
                  'email' => $request['email']
              ]);
            } else {
              $instance->user_id = $user->id;
            }
          }

          // Номер счета
          if (empty($instance->invoice)) {

            switch ($request['type']) {
              case 'horo':
                  $instance->invoice = 'Г-'.$instance->user_id.'-'.\Carbon\Carbon::parse($instance->created_at)->format('Y-m-d');
                break;
              case 'nums':
                  $instance->invoice = 'Н-'.$instance->user_id.'-'.\Carbon\Carbon::parse($instance->created_at)->format('Y-m-d');
                break;
              default:
                $instance->invoice = 'ГН-'.$instance->user_id.'-'.\Carbon\Carbon::parse($instance->created_at)->format('Y-m-d');
                break;
            }

          }
        })
    );
    return $form;
  });
});