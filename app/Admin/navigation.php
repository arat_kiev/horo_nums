<?php

use SleepingOwl\Admin\Navigation\Page;
//Группа услуг
AdminNavigation::addPage(\App\Service::class)->setTitle('Услуги')->setPages(function(Page $page) {
  $page
      ->addPage(\App\Service::class)
      ->setTitle('Услуги')
      ->setUrl('/admin/services')
      ->setPriority(0)
      ->setIcon('fa fa-bars');

  $page
      ->addPage(\App\Set::class)
      ->setTitle('Наборы услуг')
      ->setUrl('/admin/sets')
      ->setPriority(1)
      ->setIcon('fa fa-bars');
})->setIcon('fa fa-sticky-note-o')->setPriority(0);

// Группа заказов
 AdminNavigation::addPage(\App\Order::class)->setTitle('Заказы')->setPages(function(Page $page) {
 	  $page
		  ->addPage(\App\Order::class)
      ->setTitle('Список заказов')
		  ->setUrl('/admin/orders')
		  ->setPriority(2)
      ->setIcon('fa fa-shopping-basket');

   $page
       ->addPage(\App\OrderStatus::class)
       ->setTitle('Статусы заказов')
       ->setUrl('/admin/order_statuses')
       ->setPriority(3)
       ->setIcon('fa fa-bars');
 })->setIcon('fa fa-shopping-bag')->setPriority(1);

// Группа настроек
AdminNavigation::addPage(\App\Config::class)->setTitle('Настройки')->setPages(function(Page $page) {
  $page
      ->addPage(\App\Config::class)
      ->setTitle('Общие настройки')
      ->setUrl('/admin/configs')
      ->setPriority(4)
      ->setIcon('fa fa-cog');

  $page
      ->addPage(\App\Payment::class)
      ->setTitle('Платежные системы')
      ->setUrl('/admin/payments')
      ->setPriority(5)
      ->setIcon('fa fa-money');
})->setIcon('fa fa-cogs')->setPriority(99);