<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;
use App\ServiceType;

AdminSection::registerModel(App\OrderStatus::class, function (ModelConfiguration $model) {
  $model->setTitle('');
  // Создание записи
  $model->setMessageOnCreate('Заказ создан');
  // Редактирование записи
  $model->setMessageOnUpdate('Заказ обновлен');
  // Удаление записи
  $model->setMessageOnDelete('Заказ удален');
  // Восстановление записи
  $model->setMessageOnRestore('Заказ восстановлен');
  // Display
  $model->onDisplay(function () {
//    $display = AdminDisplay::datatables()->setColumns([
//        AdminColumn::text('id')->setLabel('Номер'),
//        AdminColumn::text('user.name')->setLabel('Пользователь'),
//        AdminColumn::text('user.email')->setLabel('Email'),
//        AdminColumn::text('user.phone')->setLabel('Телефон'),
//        AdminColumn::text('status.name')->setLabel('Статус'),
//        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
//    ]);
//    $display->with('user');
//    $display->with('status');
//
//    return $display;
  });
  // Edit
  $model->onEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::custom()->setDisplay(function($instance) {
            return $instance->name;
        })
    );
    return $form;
  });
});