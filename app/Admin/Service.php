<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\ServiceType;
use App\Service;

AdminSection::registerModel(App\Service::class, function (ModelConfiguration $model) {
  $model->setTitle('Услуги');
  // Создание записи
  $model->setMessageOnCreate('Услуга создана');
  // Редактирование записи
  $model->setMessageOnUpdate('Услуга обновлена');
  // Удаление записи
  $model->setMessageOnDelete('Услуга удалена');
  // Восстановление записи
  $model->setMessageOnRestore('Услуга восстановлена');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatables()->setColumns([
        AdminColumn::text('id')->setLabel('#'),
        AdminColumn::link('name')->setLabel('Название'),
        AdminColumn::text('type.name')->setLabel('Тип'),
        AdminColumn::custom('Родительская услуга', function(Model $model) {
          $root_service = Service::find($model->main_service);
          if (!empty($root_service->name)) {
            return $root_service->name;
          }
        }),
        AdminColumn::text('sort')->setLabel('Порядок сортировки'),
        AdminColumn::custom('Статус', function(Model $model) {
          $label = $model->enabled == true ? '<span class="label label-success">Опубликовано</span>' : '<span class="label label-danger">Не опубликовано</span>';
          return $label;
        }),
        AdminColumn::image('cover', 'Обложка'),
        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
    ]);
    $display->with('type');
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Заголовок')->required()->unique(),
        AdminFormElement::text('meta_title', 'Мета заголовок (SEO)'),
        AdminFormElement::textarea('meta_description', 'Мета описание (SEO)'),
        AdminFormElement::text('slug', 'Ссылка (алиас)'),
        AdminFormElement::select('service_type_id', 'Тип услуги')->setOptions(ServiceType::types())->required(),
        AdminFormElement::checkbox('root_service', 'Родительская услуга'),
        AdminFormElement::select('main_service', 'Выбор родительской услуги')->setOptions(Service::rootService()),
        AdminFormElement::text('price', 'Цена')->required(),
        AdminFormElement::text('old_price', 'Старая цена'),
        AdminFormElement::image('cover', 'Обложка'),
        AdminFormElement::ckeditor('body', 'Описание услуги')->required(),
        AdminFormElement::text('sort', 'Порядок сортировки'),
        AdminFormElement::checkbox('enabled', 'Опубликовать'),
        AdminFormElement::custom()->setCallback(function($instance) {
          if (empty($instance->alias)) {
            $instance->slug = Slug::convert($instance->name, '-');
          }
        })
    );
    return $form;
  });
});