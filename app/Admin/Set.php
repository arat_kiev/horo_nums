<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\ServiceType;
use App\Service;
use App\Set;

AdminSection::registerModel(App\Set::class, function (ModelConfiguration $model) {
  $model->setTitle('Наборы услуг');
  // Создание записи
  $model->setMessageOnCreate('Набор услуг создан');
  // Редактирование записи
  $model->setMessageOnUpdate('Набор услуг обновлен');
  // Удаление записи
  $model->setMessageOnDelete('Набор услуг удален');
  // Восстановление записи
  $model->setMessageOnRestore('Набор услуг восстановлен');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatables()->setColumns([
        AdminColumn::text('id')->setLabel('#'),
        AdminColumn::link('name')->setLabel('Название'),
        AdminColumn::custom('Статус', function(Model $model) {
          $label = $model->enabled == true ? '<span class="label label-success">Опубликовано</span>' : '<span class="label label-danger">Не опубликовано</span>';
          return $label;
        }),
        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
    ]);
    $display->with('services');
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Заголовок')->required()->unique(),
        AdminFormElement::text('price', 'Цена')->required(),
        AdminFormElement::text('old_price', 'Старая цена'),
        AdminFormElement::view('admin.services.sets_services', [], function() {}),
        AdminFormElement::custom()->setDisplay(function() {
          return AdminFormElement::multiselect('service_id', 'Услуги в наборе')
              ->setModelForOptions(App\Service::class)
              ->setDisplay('name')
              ->setLoadOptionsQueryPreparer(function($element, $query) {
                return $query
                    ->where('root_service', false)
                    ->where('enabled', true);
              })
              ->required();
        })->setCallback(function($instance) {
            $request = \Request::all();
            if (!empty($request['service_id'])) {
              if (!empty($instance->id)) {
                $set_id = $instance->id;
                \DB::table('service_set')->where('set_id', $instance->id)->delete();
              } else {
                $_set_id = Set::orderBy('id', 'desc')->first();
                $set_id = $_set_id->id + 1;
              }
              foreach ($request['service_id'] as $service_id) {
                \DB::table('service_set')->insert([
                    'set_id' => $set_id,
                    'service_id' => $service_id
                ]);
              }
            }
        }),
        AdminFormElement::checkbox('hot', 'Гарячее предложение'),
        AdminFormElement::checkbox('enabled', 'Опубликовать')
    );
    return $form;
  });
});