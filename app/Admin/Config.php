<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(App\Config::class, function (ModelConfiguration $model) {
  $model->setTitle('Общие настройки');
  // Создание записи
  $model->setMessageOnCreate('Общие настройки добавлены');
  // Редактирование записи
  $model->setMessageOnUpdate('Общие настройки обновлены');
  // Удаление записи
  $model->setMessageOnDelete('Общие настройки удалены');
  // Восстановление записи
  $model->setMessageOnRestore('Общие настройки восстановлены');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatablesAsync()->setColumns([
        AdminColumn::link('title')->setLabel('Название'),
        AdminColumn::text('email')->setLabel('email')
    ]);
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('title', 'Название')->required(),
        AdminFormElement::text('meta_title', 'Мета заголовок'),
        AdminFormElement::textarea('meta_keywords', 'Мета ключевые слова')->required(),
        AdminFormElement::textarea('meta_description', 'Мета описание')->required(),
        AdminFormElement::text('phone', 'Телефон'),
        AdminFormElement::text('email', 'Email'),
        AdminFormElement::text('address', 'Адрес'),
        AdminFormElement::text('facebook', 'Facebook'),
        AdminFormElement::text('vk', 'VK'),
        AdminFormElement::text('twitter', 'Twitter')
    );
    return $form;
  });
});