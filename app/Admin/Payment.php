<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(App\Payment::class, function (ModelConfiguration $model) {
  $model->setTitle('Настройки оплаты');
  // Создание записи
  $model->setMessageOnCreate('Платежная система добавлена');
  // Редактирование записи
  $model->setMessageOnUpdate('Платежная система обновлена');
  // Удаление записи
  $model->setMessageOnDelete('Платежная система удалена');
  // Восстановление записи
  $model->setMessageOnRestore('Платежная система восстановлена');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatablesAsync()->setColumns([
        AdminColumn::link('name')->setLabel('Название'),
        AdminColumn::custom('Статус', function(Model $model) {
          $label = $model->enabled == true ? '<span class="label label-success">Включено</span>' : '<span class="label label-danger">Выключено</span>';
          return $label;
        })
    ]);
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Название')->required()->unique(),
        AdminFormElement::text('merchant', 'Мерчант')->required(),
        AdminFormElement::text('private_key', 'Публичный ключ')->required(),
        AdminFormElement::text('public_key', 'Приватный ключ')->required(),
        AdminFormElement::checkbox('enabled', 'Включить')
    );
    return $form;
  });
});