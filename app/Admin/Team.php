<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(App\Team::class, function (ModelConfiguration $model) {
  $model->setTitle('Команда');
  // Создание записи
  $model->setMessageOnCreate('Член команды добавлен');
  // Редактирование записи
  $model->setMessageOnUpdate('Член команды обновлен');
  // Удаление записи
  $model->setMessageOnDelete('Член команды удален');
  // Восстановление записи
  $model->setMessageOnRestore('Член команды восстановлен');
  // Display
  $model->onDisplay(function () {
    $display = AdminDisplay::datatablesAsync()->setColumns([
        AdminColumn::text('id')->setLabel('#'),
        AdminColumn::link('name')->setLabel('Имя'),
        AdminColumn::custom('Деятельность', function(Model $model) {
          if ($model->staff == 'horo') {
            return 'Астрология';
          } else {
            return 'Нумерология';
          }
        }),
        AdminColumn::image('photo', 'Фото'),
        AdminColumn::custom('Статус', function(Model $model) {
          $label = $model->enabled == true ? '<span class="label label-success">Опубликовано</span>' : '<span class="label label-danger">Не опубликовано</span>';
          return $label;
        }),
        AdminColumn::datetime('updated_at')->setLabel('Изменения'),
    ]);
    return $display;
  });
  // Create And Edit
  $model->onCreateAndEdit(function() {
    $form = AdminForm::panel()->addBody(
        AdminFormElement::text('name', 'Имя')->required(),
        AdminFormElement::select('staff', 'Вид деятельности')->setOptions(['horo' => 'Астрология', 'numbers' => 'Нумерология'])->required(),
        AdminFormElement::ckeditor('description', 'Описание')->required(),
        AdminFormElement::image('photo', 'Фото'),
        AdminFormElement::checkbox('enabled', 'Опубликовать')
    );
    return $form;
  });
}) ->addMenuPage(App\Team::class, 2)
    ->setIcon('fa fa-users');