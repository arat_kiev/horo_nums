<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = ['order_id', 'product_id', 'product_type', 'quantity'];
    public $timestamps = false;

    public function order() {
      return $this->belongsTo('App\Order');
    }

    public function service() {
      return $this->belongsTo('App\Service', 'product_id');
    }

    public function set() {
      return $this->belongsTo('App\Set', 'product_id');
    }
}
