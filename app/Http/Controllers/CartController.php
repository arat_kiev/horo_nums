<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Gloudemans\Shoppingcart\Facades\Cart;
use Mail;

use App\User;
use App\Service;
use App\Payment;
use App\Set;
use App\Order;
use App\OrderProduct;
use App\Payments\WayForPay;

/**
 * Class CartController
 * @package App\Http\Controllers
 */
class CartController extends Controller
{
  public function index()
  {
    if (Cart::count() < 1) {
      return redirect('/#services');
    }
    return view('cart.cart', ['cart_items' => Cart::content()]);
  }


  /**
   * @param Request $request
   * @return mixed
   */
  public function add(Request $request)
  {
    $service_id = $request->has('service_id') ? $request->input('service_id') : null;
    $set_id = $request->has('set_id') ? $request->input('set_id') : null;

    if (!empty($service_id)) {
      $service = Service::find($service_id);
      //Добавляем в корзину товар
      Cart::add($service->id, $service->name, 1, $service->price, [])->associate('App\Service');

    } elseif (!empty($set_id)) {
      $set_id = substr($set_id, 1);
      $set = Set::find($set_id);
      //Добавляем в корзину набор
      Cart::add('0' . $set->id, $set->name, 1, $set->price, [])->associate('App\Set');
    }

    return Response::json(Cart::count());
  }

  /**
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
   */
  public function checkout(Request $request)
  {

    if (Cart::count() > 0) {
      return view('cart.checkout', ['cart_items' => Cart::content(), 'user' => Auth::user()]);
    } else {
      return redirect('/#services');
    }

  }

  /**
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function order()
  {
    if (Cart::count() > 0 && Auth::user()) {
      $order = [
          'user_id' => Auth::user()->id,
          'status_id' => 1,
          'invoice',
          'total' => Cart::subtotal(),
          'payed' => 0
      ];

      $order = Order::create($order);

      foreach (Cart::content() as $product) {
        switch ($product->associatedModel) {
          case 'App\Service':
            $type = 'service';
            break;
          case 'App\Set':
            $type = 'set';
            break;
          default:
            $type = null;
            break;
        }

        $order_product = [
            'order_id' => $order->id,
            'product_id' => $product->id,
            'product_type' => $type,
            'quantity' => $product->qty
        ];

        OrderProduct::create($order_product);
      }

      if ($order->id) {
        //Шлем уведомления о новом заказе
        $user = User::find($order->user_id);

        if (!empty($user->id)) {
          // todo: unhardcode me!
          Mail::send('mail.orders.new_order_report', ['user' => $user, 'order' => $order], function ($m) use ($user) {
            $m->from('support@kosmogram.com.ua', 'Космограмма');
            $m->to($user->email, $user->name)->subject('Ваш заказ успешно получен!');
          });

          Mail::send('mail.orders.new_order_admin_report', ['user' => $user, 'order' => $order], function ($m) use ($user) {
            $m->from('support@kosmogram.com.ua', 'Космограмма');
            $m->to('info@kosmogram.com.ua', 'Космограмма')->subject('Получен новый заказ');
          });
        }

        Cart::destroy();

        return redirect('cart/payment/' . $order->id);
      } else {
        abort(503);
      }
    } else {
      return redirect('/#services');
    }
  }

  /**
   * @param $order_id
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function payment($order_id)
  {
    $order = Order::findOrFail($order_id);

    // если найден заказ - продолжаем. Если нет - возващаем в личный кабинет
    if (!empty($order->id)) {
      // Ищем пользователя
      $user = User::findOrFail($order->user_id);

      if (!empty($user->id)) {
        // берем настройки платежной системы
        $payment = Payment::where('name', 'WayForPay')->where('enabled', true)->first();

        // Если есть данные по платежной системе - формируем форму платежки
        if (!empty($payment->id)) {
          $wfp = new WayForPay($payment->merchant, $payment->public_key);
          // массив с параметрами для запроса

          $payment_params = [
              'merchantAccount' => $payment->merchant,
              'merchantAuthType' => 'SimpleSignature',
              'merchantDomainName' => 'kosmogram.com.ua',
//              'merchantTransactionType' => 'AUTH',
              'merchantTransactionType' => 'SALE',
              'merchantTransactionSecureType' => 'AUTO',
              'serviceUrl' => 'http://kosmogram.com.ua/payment/order/'.$order->id,
              'orderReference' => $order->id,
              'orderDate' => Carbon::now()->timestamp,
              'amount' => $order->total,
              'currency' => 'UAH',
              // блокировка денег на 5 дней (в секундах)
//              'holdTimeout' => 432000
          ];

          foreach ($order->products as $product) {
            if ($product->product_type == 'service') {
              $product = Service::find($product->product_id);
            } else {
              $product = Set::find($product->product_id);
            }

            $payment_params['productName'][] = $product->name;
            $payment_params['productPrice'][] = $product->price;
            $payment_params['productCount'][] = 1;
          }
          $payment_params['defaultPaymentSystem'] = 'card';

          $form = $wfp->buildForm($payment_params);

          if (!empty($form)) {
            return view('cart.payment', ['payment_form' => $form, 'order' => $order]);
          } else {
            return redirect('cart/checkout');
          }

        }

      } else {
        return redirect('profile');
      }

    } else {
      return redirect('profile');
    }
  }

  public
  function destroy()
  {
    Cart::destroy();

    return redirect('cart');
  }

  public
  function drop($row_id)
  {
    Cart::remove($row_id);

    return redirect('cart');
  }
}
