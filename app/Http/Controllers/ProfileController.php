<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Order;
use App\OrderProduct;
use App\OrderStatus;

class ProfileController extends Controller
{


  public function getProfile() {
    $user = Auth::user();
    $orders = $user->orders;

    return view('profile', compact('user', 'orders'));
  }

  public function updateProfile(Request $request) {

    if (!empty($request->input('id'))) {
      $user = User::find($request->input('id'))->update([
        'name' => $request->input('name'),
        'last_name' => $request->input('last_name'),
        'middle_name' => $request->input('middle_name'),
        'birth_date' => $request->input('birth_date'),
        'birth_time' => $request->input('birth_time'),
        'birth_place' => $request->input('birth_place'),
        'sex' => $request->input('sex'),
        'email' => $request->input('email'),
      ]);
      return redirect('profile')->with('status1', 'Profile updated!');
    } else {
      return abort(503);
    }
  }
}
