<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Page;
use App\Service;
use App\Set;

class HomeController extends Controller
{
    public function index() {
        $pages = Page::where('enabled', true)->orderBy('sort')->get();

        foreach ($pages as $page) {
            $data['pages'][$page->alias] = [
                'title' => $page->name,
                'body' => $page->body
            ];
        }
        //Дерево нумерологии
        $_numerics = Service::where('enabled', true)
            ->where('service_type_id', 1)
            ->where('root_service', true)
            ->get();
        $numerics = [];
        foreach ($_numerics as $numeric) {
          $sub_numerics = Service::where('enabled', true)
              ->where('main_service', $numeric->id)
              ->get();
          $numerics[$numeric->id] = [
              'root' => $numeric,
              'sub_services' => $sub_numerics
          ];
        }

        //Дерево астрологии
        $_horos = Service::where('enabled', true)
            ->where('service_type_id', 2)
            ->where('root_service', true)
            ->get();
        $horos = [];
        foreach ($_horos as $horo) {
          $sub_horos = Service::where('enabled', true)
              ->where('main_service', $horo->id)
              ->get();
          $horos[$horo->id] = [
              'root' => $horo,
              'sub_services' => $sub_horos
          ];
        }

        $sets = Set::all();

        $team = Team::where('enabled', true)->get();

        return view('index', [
            'data' => $data,
            'numerics' => $numerics,
            'horos' => $horos,
            'sets' => $sets,
            'team' => $team
        ]);
    }
}
