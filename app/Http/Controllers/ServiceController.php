<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Service;

class ServiceController extends Controller
{
    public function getService($slug) {
      $service = Service::where('enabled', true)
          ->where('slug', $slug)
          ->first();

      $other_services = Service::where('enabled', true)
          ->where('id', '!=', $service->id)
          ->where('service_type_id', $service->service_type_id)
          ->where('service_type_id', '!=', null)
          ->where('root_service', false)
          ->take(6)
          ->get();

      return view('service', ['service' => $service, 'other_services' => $other_services]);
    }
}