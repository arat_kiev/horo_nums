<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Page;
use App\Service;
use App\ServiceType;

class DashboardController extends Controller
{
  //Pages
  public function getPages()
  {
    $pages = Page::orderBy('sort')->paginate(10);
    return view('admin.pages.index', ['pages' => $pages]);
  }

  public function editPage($id)
  {
    $page = Page::findOrFail($id);
    return view('admin.pages.edit', ['page' => $page]);
  }

  public function createPage(Request $request)
  {
    $page = Page::create($request->all());
    $page->slug = Slug::convert($page->name, '-');
    return redirect('/dashboard/pages');
  }

  public function updatePage(Request $request, $id)
  {
    $page = Page::findOrFail($id);
    $page->update($request->all());
    $enabled = $request->has('enabled') ? $request->input('enabled') : false;
    $blocked = $request->has('blocked') ? $request->input('blocked') : false;
    $page->enabled = $enabled;
    $page->blocked = $blocked;
    $page->slug = empty($page->slug) ? Slug::convert($page->name, '-') : $page->slug;
    $page->save();
    return redirect('/dashboard/pages');
  }

  public function dropPage($id)
  {
    $page = Page::findOrFail($id);
    $page->delete();
    return redirect('/dashboard/pages');
  }

  //Services
  public function getServices()
  {
    $services = Service::paginate(10);
    return view('admin.services.index', ['services' => $services]);
  }

  public function createService(Request $request)
  {
    $service = Service::create($request->all());
    $service->slug = Slug::convert($service->name, '-');
    return redirect('/dashboard/services');
  }

  public function editService($id)
  {
    $service = Service::findOrFail($id);
    $service_types = ServiceType::where('enabled', true)->get();
    return view('admin.services.edit', ['service' => $service, 'service_types' => $service_types]);
  }

  public function updateService(Request $request, $id)
  {
    $service = Service::findOrFail($id);
    $service->update($request->all());
    $enabled = $request->has('enabled') ? $request->input('enabled') : false;
    $service->enabled = $enabled;
    $service->slug = empty($service->slug) ? Slug::convert($service->name, '-') : $service->slug;
    $service->save();
    return redirect('/dashboard/services');
  }

  public function dropService($id)
  {
    $service = Service::findOrFail($id);
    $service->delete();
    return redirect('/dashboard/services');
  }
}
