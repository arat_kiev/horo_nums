<?php
use Illuminate\Http\Request;

Route::group(['middleware' => ['web']], function ($route) {
  //Front-pages routes
  $route->get('/', 'HomeController@index');
  $route->get('/service/{slug}', 'ServiceController@getService');

  //Profile
  $route->group(['prefix' => 'profile', 'middleware' => 'auth'], function($route) {
    $route->get('/', 'ProfileController@getProfile');
    $route->post('/update', 'ProfileController@updateProfile');
  });

  //Cart routes
  $route->group(['prefix' => 'cart'], function($route) {
    $route->get('/', 'CartController@index');
    $route->get('/drop/{row_id}', 'CartController@drop');
    $route->get('destroy', 'CartController@destroy');
    $route->get('checkout', 'CartController@checkout');
    $route->get('order', 'CartController@order');
    $route->post('add', 'CartController@add');
    $route->get('payment/{order_id}', 'CartController@payment');
  });

  //Payment API
  $route->group(['prefix' => 'payment'], function($route) {
    $route->post('order/{order_id}', 'PaymentController@order_payment');
  });

  // AUTH rotes
  $route->auth();
});