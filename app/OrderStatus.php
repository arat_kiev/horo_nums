<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public static function statuses() {
      $_statuses = self::where('enabled', true)->get();
      $statuses = [];

      foreach ($_statuses as $id => $status) {
        $statuses[$status->id] = $status->name;
      }
      return $statuses;
    }
}
