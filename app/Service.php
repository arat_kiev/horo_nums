<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
  protected $fillable = [
      'name',
      'meta_title',
      'meta_keywords',
      'meta_description',
      'slug',
      'body',
      'price',
      'old_price',
      'service_type_id',
      'enabled'
  ];

  public function type()
  {
    return $this->belongsTo('App\ServiceType', 'service_type_id', 'id');
  }

  public static function rootService() {
    $_services = self::where('enabled', true)->where('root_service', true)->get();
    $services = [];

    foreach ($_services as $service) {
      $services[$service->id] = $service->name;
    }
    return $services;
  }
}
