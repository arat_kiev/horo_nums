<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    public function services() {
      return $this->hasMany('App\Service', 'service_type_id');
    }

    public static function types() {
      $_types = self::where('enabled', true)->get();
      $types = [];

      foreach ($_types as $id => $type) {
        $types[$type->id] = $type->name;
      }
      return $types;
    }
}
