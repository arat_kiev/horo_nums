<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  protected $fillable = [
      'name',
      'meta_title',
      'meta_keywords',
      'meta_description',
      'sort',
      'body',
      'enabled',
      'blocked'
  ];
}
