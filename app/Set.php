<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $fillable = ['name', 'price', 'old_price', 'hot', 'enabled'];

    public function services() {
      return $this->belongsToMany('App\Service');
    }
}
