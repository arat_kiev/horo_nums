var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concatJs = require("gulp-concat-js");


gulp.task('css', function () {
  return gulp.src('public/assets/scss/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(autoprefixer({browsers: ['last 16 versions']}))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      // .pipe(rename('common.min.css'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('public/build/css/'));
});

gulp.task('js', function () {
  gulp.src('public/assets/js/*.js')
      .pipe(sourcemaps.init())
      // .pipe(concatJs({"target": "common.min.js"}))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('public/build/js/'));
});

gulp.task('watch', function() {
  gulp.watch('public/assets/scss/*.scss', ['css']);
  gulp.watch('public/assets/js/*.js', ['js']);
});

gulp.task('default', ['css', 'js', 'watch']);