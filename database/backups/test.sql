-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 10 2017 г., 01:00
-- Версия сервера: 5.6.31-log
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `horo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `alias`, `sort`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'Главная', 'home', 1, 0, NULL, NULL),
(2, 'Услуги', 'services', 2, 1, NULL, NULL),
(3, 'Команда', 'team', 4, 1, NULL, NULL),
(4, 'Цены', 'pricing', 5, 1, NULL, NULL),
(5, 'Статьи', 'blog', 6, 1, NULL, NULL),
(6, 'Контакты', 'contact', 7, 1, NULL, NULL),
(10, 'Описание', 'features', 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_30_194041_CreateTables', 1),
('2016_07_31_110745_ProductsCreate', 1),
('2016_08_15_141439_create_services', 2),
('2016_08_22_195207_table_pages', 3),
('2016_08_23_121900_create_products_tables', 4),
('2016_11_12_141119_ServicesTableUpdate', 5),
('2016_11_12_145922_UpdateServicesTable', 6),
('2016_11_25_150103_create_order_table', 7),
('2016_11_25_152904_create_order_status_table', 8),
('2016_11_30_185924_edit_servises_table', 9),
('2016_12_22_174728_create_sets_tables', 10),
('2016_12_25_184706_table_user_edit', 11),
('2016_12_25_191503_create_payments_table', 12);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `invoice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payed` tinyint(1) NOT NULL,
  `user_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `admin_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_products`
--

CREATE TABLE IF NOT EXISTS `order_products` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_statuses`
--

CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `color`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'Новый', '#00ff00', 1, '2016-11-25 18:31:45', '2016-11-25 18:31:47');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `name`, `alias`, `meta_description`, `meta_keywords`, `meta_title`, `body`, `sort`, `enabled`, `blocked`, `created_at`, `updated_at`) VALUES
(1, 'Добро пожаловать!', 'home', '', '', '', '<p>1</p>\r\n', 1, 1, 1, '2016-08-27 15:13:57', '2016-11-23 18:33:02'),
(2, 'Наши услуги', 'services', '', '', '', '', 3, 1, 0, '2016-08-27 15:13:58', '2016-11-19 17:31:54'),
(3, 'Фишки', 'features', '', '', '', '<h2>AWESOME FEATURES</h2>\r\n\r\n<p>App Layers offers a wide range of features with Beautiful Design &amp; Great Functionality. Lorem Ipsum is simply dummy text</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Amazing support</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</li>\r\n	<li>\r\n	<p>File Attachment</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n	<li>\r\n	<p>Share Photos</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n	<li>\r\n	<p>Modern design</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n</ul>\r\n\r\n<p><img alt="image" src="images/features_img.png" /></p>\r\n', 4, 1, 1, '2016-08-27 15:13:59', '2016-11-23 18:42:19'),
(4, 'Описание услуг', 'features_descr', '', '', '', '<section class="primary-bg">\r\n    <div class="container">\r\n        <div class="row">\r\n            <div class="col-md-12">\r\n\r\n                <div class="section_heading">\r\n                    <h2>Как это работает?</h2>\r\n\r\n                    <h4> App Layers offers a wide range of features with Beautiful Design & Great Functionality. Lorem Ipsum is simply dummy text</h4>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="how_it_work_m text-right">\r\n                        <a href="#"> <i class="fa fa-eye" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Listing Image </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n\r\n                    <div class="how_it_work_m text-right">\r\n                        <a href="#"> <i class="fa fa-clock-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Listing Update Time </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="workng_img">\r\n                        <img src="images/how_to_work.png" alt="image">\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="how_it_work_m text-left">\r\n                        <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> View Listing Detail </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n\r\n                    <div class="how_it_work_m text-left">\r\n                        <a href="#"> <i class="fa fa-heart-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Add to favorites </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>', 2, 1, 1, '2016-08-27 15:14:00', '2016-08-27 15:13:49'),
(5, 'Пакеты услуг', 'pricing', '', '', '', 'We ensure quality & support. People love us & we love them. Here goes some simple dummy text.', 5, 1, 0, '2016-08-27 15:14:01', '2016-08-27 15:13:50'),
(6, 'Команда', 'team', '', '', '', '<p>We&#39;re the best professionals in this. Here goes some simple dummy text. Lorem Ipsum is simply dummy text</p>\r\n', 6, 1, 0, '2016-08-27 15:14:02', '2016-11-23 18:58:49'),
(7, 'Отзывы', 'testimonial', '', '', '', 'We ensure quality & support. People love us & we love them. Here goes some simple dummy text.', 7, 1, 0, '2016-08-27 15:14:03', '2016-08-27 15:13:53'),
(8, 'Статьи', 'blog', '', '', '', 'We share our best ideas in our blog. Dummy text of the printing and typesetting industry. ', 8, 1, 0, '2016-08-27 15:14:04', '2016-08-27 15:13:54'),
(9, 'Контакты', 'contacts', '', '', '', 'Let drop a line to us & we will be in touch soon. Here goes some simple dummy text. Lorem Ipsum is simply dummy text', 9, 1, 0, '2016-08-27 15:14:05', '2016-08-27 15:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `merchant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `private_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `public_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pricing_tables`
--

CREATE TABLE IF NOT EXISTS `pricing_tables` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_groups`
--

CREATE TABLE IF NOT EXISTS `product_groups` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `pricing_table_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_types`
--

CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `root_service` tinyint(1) NOT NULL DEFAULT '0',
  `main_service` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(255) NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `service_type_id`, `root_service`, `main_service`, `name`, `meta_title`, `meta_description`, `slug`, `body`, `sort`, `cover`, `price`, `old_price`, `enabled`, `created_at`, `updated_at`) VALUES
(4, 1, 1, NULL, 'Еще тестовая услуга в нумерологии', '', '', 'eshche-testovaya-usluga-v-numerologii', '<p>эждэждэж</p>\r\n', 0, '', '100.00', '0.00', 1, '2016-12-13 17:01:47', '2016-12-13 17:01:47'),
(5, 1, 0, 4, 'Вторая тестова услуга', 'Вторая тестова услуга', '', 'vtoraya-testova-usluga', '<p>.бюб.бю.бю.</p>\r\n', 0, '', '100.00', '150.00', 1, '2016-12-13 17:02:34', '2016-12-13 19:22:40'),
(6, 2, 1, NULL, 'Гороскоп рут', '', '', 'goroskop-rut', '<p>смчсмч</p>\r\n', 0, '', '123.00', '0.00', 1, '2016-12-13 17:36:10', '2016-12-13 17:36:10'),
(7, 2, 0, 6, 'Гороскоп саб ', '', '', 'goroskop-sab', '<p>смчсмчсмсммс</p>\r\n', 0, '', '100.00', '0.00', 1, '2016-12-13 17:36:48', '2016-12-14 13:19:22'),
(9, 1, 0, 4, 'Очередная услгуа', '', '', 'ocherednaya-uslgua', '<p>эждэждэж</p>\r\n', 0, 'images/uploads/57ae9efd4849bad848e42bfbb5fec47c.jpg', '100.00', '0.00', 1, '2016-12-13 17:01:47', '2016-12-17 19:44:52'),
(10, 1, 0, 4, 'Очередная услгуа 2', '', '', 'ocherednaya-uslgua-2', '<p>эждэждэж</p>\r\n', 0, 'images/uploads/7f2039bfffe4a74f9a20dd86bc268d3e.jpg', '100.00', '0.00', 1, '2016-12-13 17:01:47', '2016-12-17 19:46:08');

-- --------------------------------------------------------

--
-- Структура таблицы `service_set`
--

CREATE TABLE IF NOT EXISTS `service_set` (
  `id` int(10) unsigned NOT NULL,
  `service_id` int(11) NOT NULL,
  `set_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service_set`
--

INSERT INTO `service_set` (`id`, `service_id`, `set_id`) VALUES
(13, 7, 3),
(14, 9, 3),
(18, 5, 4),
(19, 7, 4),
(20, 5, 1),
(21, 7, 1),
(22, 9, 1),
(23, 10, 1),
(24, 5, 2),
(25, 7, 2),
(26, 9, 2),
(27, 10, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `service_types`
--

CREATE TABLE IF NOT EXISTS `service_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service_types`
--

INSERT INTO `service_types` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'Нумерология', 1, '2016-11-12 14:43:37', '2016-11-12 14:43:39'),
(2, 'Астрология', 1, '2016-11-12 14:43:55', '2016-11-12 14:43:57');

-- --------------------------------------------------------

--
-- Структура таблицы `sets`
--

CREATE TABLE IF NOT EXISTS `sets` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `old_price` double(8,2) NOT NULL,
  `hot` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `sets`
--

INSERT INTO `sets` (`id`, `name`, `price`, `old_price`, `hot`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 'Light', 600.00, 0.00, 0, 1, '2016-12-24 13:26:18', '2016-12-24 13:26:24'),
(2, 'Gold', 800.00, 0.00, 1, 1, '2016-12-24 15:43:40', '2016-12-24 15:43:47'),
(3, 'Platinum', 1000.00, 0.00, 0, 1, '2016-12-24 15:44:27', '2016-12-24 15:44:29');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` timestamp NOT NULL,
  `birth_time` time NOT NULL,
  `birth_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `middle_name`, `birth_date`, `birth_time`, `birth_place`, `address`, `sex`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'arat', '', 'alexkruglik81@gmail.com', '', '0000-00-00 00:00:00', '00:00:00', '', '', 'male', '$2y$10$rG.f//7DOy6MGi0pMKuAEeKm9v/w0Bazqie725lcr6TAguER18GkC', '1hadYdOAMHGwaabd8v86vIWRd2ZpGgXN5gMpLFXQ9vjQ9MYi2IYe8tfbYhUk', '2016-08-24 16:03:48', '2016-12-25 15:45:52');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_sort_unique` (`sort`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_alias_unique` (`alias`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pricing_tables`
--
ALTER TABLE `pricing_tables`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pricing_tables_name_unique` (`name`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`);

--
-- Индексы таблицы `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_groups_name_unique` (`name`);

--
-- Индексы таблицы `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_types_name_unique` (`name`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_name_unique` (`name`);

--
-- Индексы таблицы `service_set`
--
ALTER TABLE `service_set`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `service_types`
--
ALTER TABLE `service_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sets`
--
ALTER TABLE `sets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `pricing_tables`
--
ALTER TABLE `pricing_tables`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `service_set`
--
ALTER TABLE `service_set`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `service_types`
--
ALTER TABLE `service_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `sets`
--
ALTER TABLE `sets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
