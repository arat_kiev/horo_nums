-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.31-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица horo.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_sort_unique` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.menus: ~7 rows (приблизительно)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `alias`, `sort`, `enabled`, `created_at`, `updated_at`) VALUES
	(1, 'Главная', 'home', 1, 0, NULL, NULL),
	(2, 'Услуги', 'services', 2, 1, NULL, NULL),
	(3, 'Команда', 'team', 4, 1, NULL, NULL),
	(4, 'Цены', 'pricing', 5, 1, NULL, NULL),
	(5, 'Статьи', 'blog', 6, 1, NULL, NULL),
	(6, 'Контакты', 'contact', 7, 1, NULL, NULL),
	(10, 'Описание', 'features', 3, 1, NULL, NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;


-- Дамп структуры для таблица horo.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.migrations: ~16 rows (приблизительно)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2016_07_30_194041_CreateTables', 1),
	('2016_07_31_110745_ProductsCreate', 1),
	('2016_08_15_141439_create_services', 2),
	('2016_08_22_195207_table_pages', 3),
	('2016_08_23_121900_create_products_tables', 4),
	('2016_11_12_141119_ServicesTableUpdate', 5),
	('2016_11_12_145922_UpdateServicesTable', 6),
	('2016_11_25_150103_create_order_table', 7),
	('2016_11_25_152904_create_order_status_table', 8),
	('2016_11_30_185924_edit_servises_table', 9),
	('2016_12_22_174728_create_sets_tables', 10),
	('2016_12_25_184706_table_user_edit', 11),
	('2016_12_25_191503_create_payments_table', 12),
	('2017_01_22_203018_edit_orders_table', 13),
	('2017_01_22_221829_edit_orders_table2', 14),
	('2017_01_24_112433_edit_orders_teble3', 15);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Дамп структуры для таблица horo.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `invoice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('horo','nums') COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `payed` tinyint(1) NOT NULL,
  `payment_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `admin_comments` text COLLATE utf8_unicode_ci NOT NULL,
  `result` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.orders: ~3 rows (приблизительно)
DELETE FROM `orders`;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `status_id`, `invoice`, `type`, `total`, `payed`, `payment_token`, `user_comments`, `admin_comments`, `result`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 'ГН-01-01', 'horo', 800.00, 1, '', 'asdasdasdasd', '2424234234', '', '2017-01-22 22:36:24', '2017-01-22 23:09:56'),
	(3, 1, 2, 'Г-1-2017-01-22 23:21:11', 'horo', 1.00, 0, '', '', '', '', '2017-01-22 23:21:11', '2017-01-22 23:21:11'),
	(4, 1, 1, 'Н-1-2017-01-22', 'nums', 0.00, 1, '', '', '', '', '2017-01-22 23:22:21', '2017-01-22 23:22:21');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Дамп структуры для таблица horo.order_products
CREATE TABLE IF NOT EXISTS `order_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.order_products: ~0 rows (приблизительно)
DELETE FROM `order_products`;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;


-- Дамп структуры для таблица horo.order_statuses
CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.order_statuses: ~0 rows (приблизительно)
DELETE FROM `order_statuses`;
/*!40000 ALTER TABLE `order_statuses` DISABLE KEYS */;
INSERT INTO `order_statuses` (`id`, `name`, `color`, `enabled`, `created_at`, `updated_at`) VALUES
	(1, 'Новый', '#00ff00', 1, '2016-11-25 20:31:45', '2016-11-25 20:31:47'),
	(2, 'В обработке', '#cecece', 1, '2017-01-22 23:59:16', '2017-01-22 23:59:18');
/*!40000 ALTER TABLE `order_statuses` ENABLE KEYS */;


-- Дамп структуры для таблица horo.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_alias_unique` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.pages: ~9 rows (приблизительно)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name`, `alias`, `meta_description`, `meta_keywords`, `meta_title`, `body`, `sort`, `enabled`, `blocked`, `created_at`, `updated_at`) VALUES
	(1, 'Добро пожаловать!', 'home', '', '', '', '<p>1</p>\r\n', 1, 1, 1, '2016-08-27 18:13:57', '2016-11-23 20:33:02'),
	(2, 'Наши услуги', 'services', '', '', '', '', 3, 1, 0, '2016-08-27 18:13:58', '2016-11-19 19:31:54'),
	(3, 'Фишки', 'features', '', '', '', '<h2>AWESOME FEATURES</h2>\r\n\r\n<p>App Layers offers a wide range of features with Beautiful Design &amp; Great Functionality. Lorem Ipsum is simply dummy text</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Amazing support</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</li>\r\n	<li>\r\n	<p>File Attachment</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n	<li>\r\n	<p>Share Photos</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n	<li>\r\n	<p>Modern design</p>\r\n	Contrary to popular belief, Lorem Ipsum is not simply random text.</li>\r\n</ul>\r\n\r\n<p><img alt="image" src="images/features_img.png" /></p>\r\n', 4, 1, 1, '2016-08-27 18:13:59', '2016-11-23 20:42:19'),
	(4, 'Описание услуг', 'features_descr', '', '', '', '<section class="primary-bg">\r\n    <div class="container">\r\n        <div class="row">\r\n            <div class="col-md-12">\r\n\r\n                <div class="section_heading">\r\n                    <h2>Как это работает?</h2>\r\n\r\n                    <h4> App Layers offers a wide range of features with Beautiful Design & Great Functionality. Lorem Ipsum is simply dummy text</h4>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="how_it_work_m text-right">\r\n                        <a href="#"> <i class="fa fa-eye" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Listing Image </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n\r\n                    <div class="how_it_work_m text-right">\r\n                        <a href="#"> <i class="fa fa-clock-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Listing Update Time </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="workng_img">\r\n                        <img src="images/how_to_work.png" alt="image">\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="col-md-4">\r\n                    <div class="how_it_work_m text-left">\r\n                        <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> View Listing Detail </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n\r\n                    <div class="how_it_work_m text-left">\r\n                        <a href="#"> <i class="fa fa-heart-o" aria-hidden="true"></i> </a>\r\n                        <a href="#"> <h5> Add to favorites </h5> </a>\r\n                        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer </p>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>', 2, 1, 1, '2016-08-27 18:14:00', '2016-08-27 18:13:49'),
	(5, 'Пакеты услуг', 'pricing', '', '', '', 'We ensure quality & support. People love us & we love them. Here goes some simple dummy text.', 5, 1, 0, '2016-08-27 18:14:01', '2016-08-27 18:13:50'),
	(6, 'Команда', 'team', '', '', '', '<p>We&#39;re the best professionals in this. Here goes some simple dummy text. Lorem Ipsum is simply dummy text</p>\r\n', 6, 1, 0, '2016-08-27 18:14:02', '2016-11-23 20:58:49'),
	(7, 'Отзывы', 'testimonial', '', '', '', 'We ensure quality & support. People love us & we love them. Here goes some simple dummy text.', 7, 1, 0, '2016-08-27 18:14:03', '2016-08-27 18:13:53'),
	(8, 'Статьи', 'blog', '', '', '', 'We share our best ideas in our blog. Dummy text of the printing and typesetting industry. ', 8, 1, 0, '2016-08-27 18:14:04', '2016-08-27 18:13:54'),
	(9, 'Контакты', 'contacts', '', '', '', 'Let drop a line to us & we will be in touch soon. Here goes some simple dummy text. Lorem Ipsum is simply dummy text', 9, 1, 0, '2016-08-27 18:14:05', '2016-08-27 18:13:55');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Дамп структуры для таблица horo.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.password_resets: ~0 rows (приблизительно)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


-- Дамп структуры для таблица horo.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `merchant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `private_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `public_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.payments: ~1 rows (приблизительно)
DELETE FROM `payments`;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `name`, `merchant`, `private_key`, `public_key`, `enabled`, `created_at`, `updated_at`) VALUES
	(1, 'WayForPay', 'test_merch_n1', 'flk3409refn54t54t*FNJRET', 'flk3409refn54t54t*FNJRET', 1, '2017-01-24 12:15:03', '2017-01-24 12:15:04');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


-- Дамп структуры для таблица horo.pricing_tables
CREATE TABLE IF NOT EXISTS `pricing_tables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pricing_tables_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.pricing_tables: ~0 rows (приблизительно)
DELETE FROM `pricing_tables`;
/*!40000 ALTER TABLE `pricing_tables` DISABLE KEYS */;
/*!40000 ALTER TABLE `pricing_tables` ENABLE KEYS */;


-- Дамп структуры для таблица horo.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.products: ~0 rows (приблизительно)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


-- Дамп структуры для таблица horo.product_groups
CREATE TABLE IF NOT EXISTS `product_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `pricing_table_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.product_groups: ~0 rows (приблизительно)
DELETE FROM `product_groups`;
/*!40000 ALTER TABLE `product_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_groups` ENABLE KEYS */;


-- Дамп структуры для таблица horo.product_types
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_types_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.product_types: ~0 rows (приблизительно)
DELETE FROM `product_types`;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;


-- Дамп структуры для таблица horo.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `root_service` tinyint(1) NOT NULL DEFAULT '0',
  `main_service` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(255) NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `services_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.services: ~6 rows (приблизительно)
DELETE FROM `services`;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `service_type_id`, `root_service`, `main_service`, `name`, `meta_title`, `meta_description`, `slug`, `body`, `sort`, `cover`, `price`, `old_price`, `enabled`, `created_at`, `updated_at`) VALUES
	(4, 1, 1, NULL, 'Еще тестовая услуга в нумерологии', '', '', 'eshche-testovaya-usluga-v-numerologii', '<p>эждэждэж</p>\r\n', 0, '', 100.00, 0.00, 1, '2016-12-13 19:01:47', '2016-12-13 19:01:47'),
	(5, 1, 0, 4, 'Вторая тестова услуга', 'Вторая тестова услуга', '', 'vtoraya-testova-usluga', '<p>.бюб.бю.бю.</p>\r\n', 0, '', 100.00, 150.00, 1, '2016-12-13 19:02:34', '2016-12-13 21:22:40'),
	(6, 2, 1, NULL, 'Гороскоп рут', '', '', 'goroskop-rut', '<p>смчсмч</p>\r\n', 0, '', 123.00, 0.00, 1, '2016-12-13 19:36:10', '2016-12-13 19:36:10'),
	(7, 2, 0, 6, 'Гороскоп саб ', '', '', 'goroskop-sab', '<p>смчсмчсмсммс</p>\r\n', 0, '', 100.00, 0.00, 1, '2016-12-13 19:36:48', '2016-12-14 15:19:22'),
	(9, 1, 0, 4, 'Очередная услгуа', '', '', 'ocherednaya-uslgua', '<p>эждэждэж</p>\r\n', 0, 'images/uploads/57ae9efd4849bad848e42bfbb5fec47c.jpg', 100.00, 0.00, 1, '2016-12-13 19:01:47', '2016-12-17 21:44:52'),
	(10, 1, 0, 4, 'Очередная услгуа 2', '', '', 'ocherednaya-uslgua-2', '<p>эждэждэж</p>\r\n', 0, 'images/uploads/7f2039bfffe4a74f9a20dd86bc268d3e.jpg', 100.00, 0.00, 1, '2016-12-13 19:01:47', '2016-12-17 21:46:08');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;


-- Дамп структуры для таблица horo.service_set
CREATE TABLE IF NOT EXISTS `service_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `set_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.service_set: ~11 rows (приблизительно)
DELETE FROM `service_set`;
/*!40000 ALTER TABLE `service_set` DISABLE KEYS */;
INSERT INTO `service_set` (`id`, `service_id`, `set_id`) VALUES
	(13, 7, 3),
	(14, 9, 3),
	(18, 5, 4),
	(19, 7, 4),
	(20, 5, 1),
	(21, 7, 1),
	(22, 9, 1),
	(23, 10, 1),
	(24, 5, 2),
	(25, 7, 2),
	(26, 9, 2),
	(27, 10, 2);
/*!40000 ALTER TABLE `service_set` ENABLE KEYS */;


-- Дамп структуры для таблица horo.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.service_types: ~2 rows (приблизительно)
DELETE FROM `service_types`;
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
	(1, 'Нумерология', 1, '2016-11-12 16:43:37', '2016-11-12 16:43:39'),
	(2, 'Астрология', 1, '2016-11-12 16:43:55', '2016-11-12 16:43:57');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;


-- Дамп структуры для таблица horo.sets
CREATE TABLE IF NOT EXISTS `sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `old_price` double(8,2) NOT NULL,
  `hot` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.sets: ~4 rows (приблизительно)
DELETE FROM `sets`;
/*!40000 ALTER TABLE `sets` DISABLE KEYS */;
INSERT INTO `sets` (`id`, `name`, `price`, `old_price`, `hot`, `enabled`, `created_at`, `updated_at`) VALUES
	(1, 'Light', 600.00, 0.00, 0, 1, '2016-12-24 15:26:18', '2016-12-24 15:26:24'),
	(2, 'Gold', 800.00, 0.00, 1, 1, '2016-12-24 17:43:40', '2016-12-24 17:43:47'),
	(3, 'Platinum', 1000.00, 0.00, 0, 1, '2016-12-24 17:44:27', '2016-12-24 17:44:29');
/*!40000 ALTER TABLE `sets` ENABLE KEYS */;


-- Дамп структуры для таблица horo.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `birth_time` time NOT NULL,
  `birth_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы horo.users: ~1 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `middle_name`, `birth_date`, `birth_time`, `birth_place`, `sex`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'arat', 'Admin', 'alexkruglik81@gmail.com', 'Adminovich', '2017-01-21', '05:45:00', 'Украина, Киев', 'male', '$2y$10$rG.f//7DOy6MGi0pMKuAEeKm9v/w0Bazqie725lcr6TAguER18GkC', '1hadYdOAMHGwaabd8v86vIWRd2ZpGgXN5gMpLFXQ9vjQ9MYi2IYe8tfbYhUk', '2016-08-24 19:03:48', '2017-01-22 21:49:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
