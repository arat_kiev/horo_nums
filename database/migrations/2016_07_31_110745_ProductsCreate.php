<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->text('body');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('product_group_id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->text('body');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_groups');
        Schema::drop('products');
    }
}
