<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableUserEdit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name')->after('name');
            $table->string('middle_name');
            $table->timestamp('birth_date');
            $table->time('birth_time');
            $table->string('birth_place', 255);
            $table->string('address', 255);
            $table->enum('sex', ['male', 'female']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('middle_name');
            $table->dropColumn('birth_date');
            $table->dropColumn('birth_time');
            $table->dropColumn('birth_place');
            $table->dropColumn('address');
            $table->dropColumn('sex');
        });
    }
}
