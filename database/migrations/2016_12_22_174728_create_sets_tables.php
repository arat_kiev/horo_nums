<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('price');
            $table->float('old_price');
            $table->boolean('hot');
            $table->boolean('enabled');
            $table->timestamps();
        });

      Schema::create('service_set', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('service_id');
        $table->integer('set_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sets');
        Schema::drop('service_set');
    }
}
