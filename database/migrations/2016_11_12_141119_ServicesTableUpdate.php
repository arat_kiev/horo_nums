<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServicesTableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function(Blueprint $table) {
          $table->integer('service_type_id')->after('id');
        });

        Schema::create('service_types', function(Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->boolean('enabled');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function(Blueprint $table) {
          $table->dropColumn('service_type_id');
        });

        Schema::drop('service_types');
    }
}
