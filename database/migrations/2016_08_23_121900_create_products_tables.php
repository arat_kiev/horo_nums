<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_type_id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->text('body');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        Schema::create('product_types', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('product_type_id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->text('body');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        Schema::create('product_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('product_id');
            $table->integer('product_type_id');
            $table->integer('pricing_table_id');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });

        Schema::create('pricing_tables', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
        Schema::drop('product_types');
        Schema::drop('product_groups');
        Schema::drop('pricing_tables');
    }
}
