<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('status_id');
          $table->string('invoice');
          $table->boolean('payed');
          $table->text('user_comments');
          $table->text('admin_comments');
          $table->timestamps();
        });

      Schema::create('order_products', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('order_id');
        $table->integer('product_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
        Schema::drop('order_products');
    }
}
