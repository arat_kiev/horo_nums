<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен состоять не менее, чем из 6 символов.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'На Ваш адрес электронной почты выслано письмо с инструкциями по сбросу пароля.',
    'token' => 'Неверный ключ для сброса пароля.',
    'user' => "Пользователь с таким email не найден.",

];
