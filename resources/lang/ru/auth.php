<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email не зарегистрирован в системе.',
    'throttle' => 'Слишком много попытог входа на сайт. Пожалуйста, попробуйте войти на сайт через :seconds секунд.',

];
