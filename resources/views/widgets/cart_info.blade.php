{{--Minicart--}}
<div class="col-md-4">
	<aside>
		<div class="side_blog_bg">
			<div class="sidebar_wrap">
				<div class="side_bar_heading">
					<h6>Ваш заказ</h6>
				</div>
				<ul class="cart-mini-items">
					@foreach($cart_items as $cart_item)
						<li>
							@if(!empty($cart_item->model->slug))
								<a href="/service/{!! $cart_item->model->slug !!}">
									@if(!empty($cart_item->model->cover))
										<img src="/{!! $cart_item->model->cover !!}" class="img-responsive img-thumbnail" alt="{!! $cart_item->title !!}">
									@endif
									{!! $cart_item->name !!}, {!! $cart_item->price !!} грн.
								</a>
							@else
								@if(!empty($cart_item->model->cover))
									<img src="/{!! $cart_item->model->cover !!}" class="img-responsive img-thumbnail" alt="{!! $cart_item->title !!}">
								@endif
								@if(!empty($cart_item->model->slug))
									{!! $cart_item->name !!}, {!! $cart_item->price !!} грн.
								@else
									Набор {!! $cart_item->name !!}, {!! $cart_item->price !!} грн.
								@endif
							@endif
						</li>
					@endforeach
				</ul>
				<p class="cart-mini-subtotal">
					<strong>Итого: {!! Cart::subtotal() !!} грн</strong>
				</p>
				<p>
					<a href="/cart" class="edit-cart text-center"><i class="fa fa-pencil"></i> редактировать</a>
				</p>
				@if(!empty($user))
				{{--Профиль пользователя--}}
				<div class="side_bar_heading">
					<h6>Ваш профиль</h6>
				</div>
				<ul class="cart-user-info">
					<li><strong>ФИО:</strong> {!! $user->last_name !!} {!! $user->name !!} {!! $user->middle_name !!}</li>
					<li><strong>Пол:</strong>
						@if($user->sex == 'male')
							Мужчина
						@else
							Женщина
						@endif
					</li>
					<li><strong>Дата, время рождения:</strong> {!! \Carbon\Carbon::parse($user->birth_date)->format('m.d.Y') !!}, {!! \Carbon\Carbon::parse($user->birth_time)->format('H:i') !!}</li>
					<li><strong>Место рождения:</strong> {!! $user->birth_place !!}</li>
					<li><strong>Email:</strong> {!! $user->email !!}</li>
				</ul>
				<p>
					<a href="{!! url('profile') !!}" class="edit-cart text-center"><i class="fa fa-pencil"></i> редактировать</a>
				</p>
				@endif
			</div>
		</div>
	</aside>
</div>