<section id="features" class="padding_bottom_none our_service_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2>Как это работает?</h2>

                    <h4>Небольшое краткое описание того, как работают услуги, которые предоставляются.</h4>
                </div>

                <div class="col-md-5 pull-right">
                    <div class="services_detail">
                        <ul>
                            <li>
                                <a href="#"><span><i class="fa fa-html5" aria-hidden="true"></i></span>
                                    <h5>Услуга 1</h5></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>

                            <li>
                                <a href="#"><span><i class="fa fa-desktop" aria-hidden="true"></i></span>
                                    <h5>Услуга 2</h5></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>

                            <li>
                                <a href="#"><span><i class="fa fa-rocket" aria-hidden="true"></i></span>
                                    <h5>Услуга 3</h5></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                            <li>
                                <a href="#"><span><i class="fa fa-rocket" aria-hidden="true"></i></span>
                                    <h5>Услуга 4</h5></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                        </ul>

                    </div>
                </div>

                <div class="col-md-7">
                    <div class="services_img">
                        <img src="images/services_img1.png" alt="image-1">
                    </div>

                    <div class="services_img_n">
                        <img src="images/services_img2.png" alt="image-1">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
{{--{!! $page['body'] !!}--}}