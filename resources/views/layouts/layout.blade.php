<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @yield('seo')
        <title>@yield('title') | Космограмма</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('/css/bootstrap-cosmo.min.css') }}">
         <!-- CSS Custom -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/build/css/common.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/build/css/common.css') }}">

        <!-- favicon Icon -->
        <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon">
        <link rel="icon" href="/images/favicon.png" type="image/x-icon">
        <!-- CSS Plugins -->
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/animate.min.css') }}">
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        @yield('styles')
         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        @include('parts.header')
        @yield('content')
        @include('parts.footer')
				@yield('modals')
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/interface.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('build/js/cart.js') }}"></script>
        <script src="{{ asset('build/js/main.js') }}"></script>
        <script src="https://use.fontawesome.com/57f55eba2b.js"></script>
        {{-- Custom Scripts --}}
        @yield('scripts')
    </body>
</html>
