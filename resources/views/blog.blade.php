<section id="blog" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2>Наш блог</h2>

                    <h4>Если Вам интересна инфорция касательно нумерологии или астрологии, мы публикуем интересные статьи на эти темы.</h4>
                </div>


                <div class="col-md-4">
                    <article class="our_blog">
                        <div class="blog_image">
                            <img src="images/blog-img1.png" alt="image">
                        </div>

                        <div class="blog_detail">
                            <div class="category_heading">
                                <a href="#"> <h6> Marketing </h6> </a>
                                <a href="#"> <h5> Lorem Ipsum is simply dummy text of the printing. </h5> </a>

                                <ul>
                                    <li>  <i class="fa fa-clock-o" aria-hidden="true"></i>  20 Марта 2016 </li>
                                    <li> <a href="#">  <i class="fa fa-comments-o" aria-hidden="true"></i> Комментарии </a> </li>
                                </ul>

                                <a href="#"  class="read_more"> <p> Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p> </a>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-md-4">
                    <article class="our_blog">
                        <div class="blog_image">
                            <img src="images/blog-img2.png" alt="image">
                        </div>

                        <div class="blog_detail">
                            <div class="category_heading">
                                <a href="#"> <h6> Marketing </h6> </a>
                                <a href="#"> <h5> Lorem Ipsum is simply dummy text of the printing. </h5> </a>

                                <ul>
                                    <li>  <i class="fa fa-clock-o" aria-hidden="true"></i>  20 Марта 2016 </li>
                                    <li> <a href="#">  <i class="fa fa-comments-o" aria-hidden="true"></i> Комментарии </a> </li>
                                </ul>

                                <a href="#"  class="read_more"> <p> Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p> </a>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-md-4">
                    <article class="our_blog">
                        <div class="blog_image">
                            <img src="images/blog-img3.png" alt="image">
                        </div>

                        <div class="blog_detail">
                            <div class="category_heading">
                                <a href="#"> <h6> Marketing </h6> </a>
                                <a href="#"> <h5> Lorem Ipsum is simply dummy text of the printing. </h5> </a>

                                <ul>
                                    <li>  <i class="fa fa-clock-o" aria-hidden="true"></i>  20 Марта 2016 </li>
                                    <li> <a href="#">  <i class="fa fa-comments-o" aria-hidden="true"></i> Комментарии </a> </li>
                                </ul>

                                <a href="#"  class="read_more"> <p> Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p> </a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>