@extends('layouts.layout')

@section('styles')
	<link rel="stylesheet" href="{{ asset('build/css/common.min.css') }}">
@stop

@section('title')
	Вход
@stop

@section('content')
	<br>
	<section class="post_blog_bg primary-bg">
		
		<div class="container">
			<div class="row">
				<article class="blog_post cart-block">
					
					<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default auth-form">
							<div class="panel-heading">Вход на сайт</div>
							<div class="panel-body">
								<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
									{{ csrf_field() }}
									
									<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
										<label for="email" class="col-md-4 control-label">Email</label>
										
										<div class="col-md-6">
											<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
											
											@if ($errors->has('email'))
												<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
											@endif
										</div>
									</div>
									
									<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
										<label for="password" class="col-md-4 control-label">Пароль</label>
										
										<div class="col-md-6">
											<input id="password" type="password" class="form-control" name="password">
											
											@if ($errors->has('password'))
												<span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
											@endif
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="remember"> Запомнить меня
												</label>
												<label>
													<a class="btn btn-link" href="{{ url('/password/reset') }}">Забыли пароль?</a>
												</label>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<button type="submit" class="btn btn-success">
												<i class="fa fa-btn fa-sign-in"></i> Войти
											</button>
											<a href="/register" class="btn btn-primary">
												<i class="fa fa-btn fa-user-plus"></i> Регистрация
											</a>
										
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
@endsection
