<div class="page-header">
	<h4>Заказ</h4>
</div>

<table class="table table-striped">
	<thead>
	<th>Наименование</th>
	<th>Тип</th>
	<th>Цена</th>
	</thead>
	<tbody>
	@foreach($products as $product)
		@if($product->product_type == 'service')
			<tr>
				<td>{!! $product->service->name !!}</td>
				<td>Услуга</td>
				<td>{!! $product->service->price !!}</td>
			</tr>
			@else
			<tr>
				<td>{!! $product->set->name !!}</td>
				<td>Акционный набор</td>
				<td>{!! $product->set->price !!}</td>
			</tr>
		@endif
	@endforeach
	</tbody>
</table>