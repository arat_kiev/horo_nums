<div class="page-header">
	<h4>Клиент</h4>
</div>
<table class="table">
	<thead>
		<tr>
			<th>ФИО</th>
			<th>Дата, время рождения</th>
			<th>Место рождения</th>
			<th>Пол</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<td>{!! $user->name !!} {!! $user->middle_name !!} {!! $user->last_name !!}</td>
		<td>
			{!! \Carbon\Carbon::parse($user->birth_date)->format('d.m.Y') !!},
			{!! \Carbon\Carbon::parse($user->birth_time)->format('H:i') !!}
		</td>
		<td>
			{!! $user->birth_place !!}
		</td>
		<td>
			@if($user->sex == 'male')
				<span class="text-primary"><i class="fa fa-mars"></i></span>
			@else
				<span class="text-danger"><i class="fa fa-venus"></i></span>
			@endif
		</td>
		<td>{!! $user->email !!}</td>
	</tr>
	</tbody>
</table>