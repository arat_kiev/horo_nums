<div class="page-header">
	<h4>Клиент</h4>
</div>
<div class="form-group">
	<label for="userLastName">Фамилия</label>
	<input type="text" id="userLastName" class="form-control" name="user_last_name">
</div>
<div class="form-group">
	<label for="userName">Имя</label>
	<input type="text" id="userName" class="form-control" name="user_name">
</div>
<div class="form-group">
	<label for="userMiddleName">Отчество</label>
	<input type="text" id="userMiddleName" class="form-control" name="user_middle_name">
</div>
<div class="form-group">
	<label for="birthDate">Дата рождения</label>
	<input type="date" id="birthDate" class="form-control" name="user_birth_date">
</div>
<div class="form-group">
	<label for="birthTime">Время рождения</label>
	<input type="time" id="birthTime" class="form-control" name="user_birth_time">
</div>
<div class="form-group">
	<label for="userEmail">Email</label>
	<input type="email" id="userEmail" class="form-control" name="user_email">
</div>