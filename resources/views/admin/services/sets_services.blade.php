<div class="page-header">
	<h4>Услуги в наборе {!! $model->name !!}</h4>
</div>
<table class="table table-striped">
	<thead>
		<th>ID</th>
		<th>Услуга</th>
		<th>Цена</th>
	</thead>
	<tbody>
		@foreach($model->services as $service)
			<tr>
				<td>{!! $service->id !!}</td>
				<td>{!! $service->name !!}</td>
				<td>{!! $service->price !!} грн.</td>
			</tr>
		@endforeach
	</tbody>
</table>