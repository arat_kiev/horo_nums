@extends('layouts.layout')

@section('title')
    Космограмма
@endsection

@section('content')
    {{--Выбираем из базы все доступные страницы и выводим их шаблоны--}}
    {{--@foreach($data['pages'] as $key => $pages)--}}
        {{--@foreach($pages as $alias => $page)--}}
            {{--@include($alias)--}}
        {{--@endforeach--}}
    {{--@endforeach--}}
    @include('home')
    @include('services')
    @include('features')
    {{--@include('features_descr')--}}
    @include('team')
    @include('pricing')
    @include('blog')
    @include('testimonial')
    @include('contacts')
    @include('subscribe')

@endsection