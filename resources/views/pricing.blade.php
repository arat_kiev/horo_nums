<section id="pricing" class="price_table_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2>Пакеты наших услуг</h2>

                    <h4>Выбрав пакет наших услуг, Вы можете сэкономить до 30%!</h4>
                </div>
    
                @foreach($sets as $set)
                <div class="col-md-4">
                    <div class="table-1">
                        {{--<div class="discount">--}}
                            {{--<p> Save 10% </p>--}}
                        {{--</div>--}}

                        <h3>{!! $set->name !!}</h3>
                        @if($set->hot == true)
                            <div class="price_month_m">
                        @else
                            <div class="price_month">
                        @endif
                            <span class="round">
                                <h3> {!! $set->price !!} грн. </h3>
                                {{--<span>--}}
                                 {{--<p> /Month </p>--}}
                                {{--</span>--}}
                            </span>
                        </div>
                        <div>
                            <h6> Астролог:</h6>
                        </div>
                        <ul>
                            @foreach($set->services as $service)
                                @if($service->type->name == 'Астрология')
                                    <li>
                                        <a href="/service/{!! $service->slug !!}" class="pricing-table-link">
                                        {!! $service->name !!}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div>
                            <h6>Нумеролог:</h6>
                        </div>
                        <ul>
                            @foreach($set->services as $service)
                                @if($service->type->name == 'Нумерология')
                                    <li>
                                        <a href="/service/{!! $service->slug !!}" class="pricing-table-link">
                                            {!! $service->name !!}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="section_sub_btn order-button">
                            <button class="btn btn-default add-to-cart" type="button" id="pricing-0{{ $set->id }}" data-set-id="0{{ $set->id }}"> <i class="fa fa-cart-plus" aria-hidden="true"></i> Заказать</button>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                </div>
                @endforeach
                <div class="col-md-12 section_heading section_heading_2">
                    <h4>Конечно же, Вы можете выбрать необходимую Вам услугу отдельно.</h4>
                </div>
            </div>
        </div>
    </div>
</section>
