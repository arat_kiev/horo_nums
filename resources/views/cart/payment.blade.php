@extends('layouts.layout')

@section('seo')
	<meta name="description" content="">
	<title>Оплатить заказ</title>
@stop

@section('content')
	<section class="post_blog_bg primary-bg">
		<div class="container">
			<div class="blog_post cart-block">
				<div class="row">
					{{--Checkout form--}}
					<div class="col-md-8 checkout-block">
						
						@if(!Auth::user())
							<div class="page-header">
								<h5>Оформление заказа</h5>
							</div>
						<div class="alert alert-info">
							<strong><i class="fa fa-info"></i></strong>
							Для оформления и получения заказа Вам необходимо зарегистрироваться и войти на сайт.
						</div>
							
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">&nbsp;</div>
									<div class="panel-body">
										<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
											{{ csrf_field() }}
											
											<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
												<label for="email" class="col-md-4 control-label">Email</label>
												
												<div class="col-md-6">
													<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
													
													@if ($errors->has('email'))
														<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
													@endif
												</div>
											</div>
											
											<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
												<label for="password" class="col-md-4 control-label">Пароль</label>
												
												<div class="col-md-6">
													<input id="password" type="password" class="form-control" name="password">
													
													@if ($errors->has('password'))
														<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
													@endif
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 col-md-offset-4">
													<div class="checkbox">
														<label>
															<input type="checkbox" name="remember"> Запомнить меня
														</label>
														<label>
															<a class="btn btn-link" href="{{ url('/password/reset') }}">Забыли пароль?</a>
														</label>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 col-md-offset-4">
													<button type="submit" class="btn btn-success">
														<i class="fa fa-btn fa-sign-in"></i> Войти
													</button>
													<a href="/register" class="btn btn-primary">
														<i class="fa fa-btn fa-user-plus"></i> Регистрация
													</a>
													
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						@else
							<div class="page-header">
								<h5>4. Оплата заказа</h5>
							</div>
							<p>
								Средства будут заблокированы на Вашей банковской карте до получения выбранных услуг.
								Средства блокируются на максимальный срок в 5 банковских дней. В этот срок мы гарантируем получение Вами выбранных услуг.
							</p>
							{!! $payment_form !!}
						@endif
							
					</div>
					
					{{--Minicart--}}
					<div class="col-md-4">
						<aside>
							<div class="side_blog_bg">
								<div class="sidebar_wrap">
									<div class="side_bar_heading">
										<h6>Спасибо!</h6>
									</div>
									<p class="text-center">
										<strong>Ваш заказ №{!! $order->id !!} успешно создан.</strong>
									</p>
									<p class="text-center">
										Если Вы не оплатили заказ, то Вы можете это сделать на данной странице.
									</p>
									{{--todo: вывести товары заказа в список--}}
									<div class="side_bar_heading">
										<h6>Детали заказа</h6>
									</div>
									<ul class="cart-mini-items">
										@foreach($order->products as $product)
											<li>
											@if($product->product_type == 'service')
													<a href="/service/{!! $product->service->slug !!}">
														@if(!empty($product->service->cover))
															<img src="/{!! $product->service->cover !!}" class="img-responsive img-thumbnail" alt="{!! $product->service->name !!}">
														@endif
															{!! $product->service->name !!}, {!! $product->service->price !!} грн.
													</a>
											@else
													Пакет {!! $product->set->name !!}, {!! $product->set->price !!} грн.
											@endif
											</li>
										@endforeach
									</ul>
									<p class="cart-mini-subtotal">
										<strong>Итого: {!! $order->total !!} грн.</strong>
									</p>
								</div>
							</div>
						</aside>
					</div>
					
				</div>
			</div>
		</div>
	</section>
@stop

@section('scripts')
	<script src="/build/js/cart.js"></script>
@stop