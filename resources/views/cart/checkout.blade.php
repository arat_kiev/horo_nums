@extends('layouts.layout')

@section('seo')
	<meta name="description" content="">
	<title>Оформить заказ</title>
@stop

@section('content')
	<section class="post_blog_bg primary-bg">
		<div class="container">
			<div class="blog_post cart-block">
				<div class="row">
					{{--Checkout form--}}
					<div class="col-md-8 checkout-block">
						
						@if(!Auth::user())
							<div class="page-header">
								<h5>Оформление заказа</h5>
							</div>
						<div class="alert alert-info">
							<strong><i class="fa fa-info"></i></strong>
							Для оформления и получения заказа Вам необходимо зарегистрироваться и войти на сайт.
						</div>
							
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">&nbsp;</div>
									<div class="panel-body">
										<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
											{{ csrf_field() }}
											
											<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
												<label for="email" class="col-md-4 control-label">Email</label>
												
												<div class="col-md-6">
													<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
													
													@if ($errors->has('email'))
														<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
													@endif
												</div>
											</div>
											
											<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
												<label for="password" class="col-md-4 control-label">Пароль</label>
												
												<div class="col-md-6">
													<input id="password" type="password" class="form-control" name="password">
													
													@if ($errors->has('password'))
														<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
													@endif
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 col-md-offset-4">
													<div class="checkbox">
														<label>
															<input type="checkbox" name="remember"> Запомнить меня
														</label>
														<label>
															<a class="btn btn-link" href="{{ url('/password/reset') }}">Забыли пароль?</a>
														</label>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-6 col-md-offset-4">
													<button type="submit" class="btn btn-success">
														<i class="fa fa-btn fa-sign-in"></i> Войти
													</button>
													<a href="/register" class="btn btn-primary">
														<i class="fa fa-btn fa-user-plus"></i> Регистрация
													</a>
													
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						@else
							<div class="page-header">
								<h5>2. Проверка профиля</h5>
							</div>
							<p>
								Для получения Вашего заказа, пройдите следующие шаги:
							</p>
							<ol class="checkout-instruction">
								<li>
									<i class="fa fa-address-card-o text-primary checkout-block-icon"></i>
									Проверить регистрационные данные из своего <a href="{!! url('profile') !!}">личного кабинета</a>.
								</li>
								<li>
									<i class="fa fa-plus text-info checkout-block-icon"></i>
									Подтвердить свой заказ
								</li>
								<li>
									<i class="fa fa-credit-card text-warning checkout-block-icon"></i>
									<a href="{!! url('cart/payment') !!}">Оплатить заказ</a> с помощью сервиса онлайн оплаты. Вы можете оплатить с помощью любой банковской карты Visa/Master Card.
									Средства будут заблокированы на Вашей банковской карте до получения выбранных услуг.
									Средства блокируются на максимальный срок в 5 банковских дней. В этот срок мы гарантируем получение Вами выбранных услуг.
								</li>
								<li>
									<i class="fa fa-check text-success checkout-block-icon"></i>
									Получить заказ в личном кабинете.
								</li>
							</ol>

							<div class="page-header">
								<h5>3. Подтверждение заказа</h5>
							</div>
							<a href="{!! url('cart/order') !!}" class="btn btn-success"><i class="fa fa-check"></i> Подтвердить заказ</a>
						@endif
							
					</div>
					{{--{{ Widget::cartItem() }}--}}
					{{--@widget('cartInfo')--}}
					@include('widgets.cart_info')
					
				</div>
			</div>
		</div>
	</section>
@stop

@section('scripts')
	<script src="/build/js/cart.js"></script>
@stop