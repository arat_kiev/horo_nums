@extends('layouts.layout')

@section('seo')
	<meta name="description" content="">
	<title>Корзина</title>
@stop

@section('styles')
	<link rel="stylesheet" href="{!! asset('build/css/common.min.css') !!}">
@stop

@section('content')
	<section class="post_blog_bg primary-bg">
		<div class="container">
			<div class="row">
				<article class="blog_post cart-block">
					<div class="page-header">
						<h5>1. Проверка заказа</h5>
					</div>
					<table class="table table-striped">
						<thead>
							<th>Услуга</th>
							<th>Количество</th>
							<th>Цена</th>
							<th>Стоимость</th>
							<th></th>
						</thead>
						<tbody>
							@foreach($cart_items as $cart_item)
							<tr>
								<td>
									@if(!empty($cart_item->model->cover))
										<img src="/{!! $cart_item->model->cover !!}" class="img-responsive img-thumbnail cart-item-image" alt="{!! $cart_item->title !!}" style="max-width: 100px;">
									@endif
										@if(!empty($cart_item->model->slug))
										{!! $cart_item->name !!}
											@else
										Набор {!! $cart_item->name !!}
										@endif
								</td>
								<td>{!! $cart_item->qty !!}</td>
								<td>{!! $cart_item->price !!} грн</td>
								<td>{!! $cart_item->price * $cart_item->qty !!} грн</td>
								<td>
									<a href="/cart/drop/{!! $cart_item->rowId !!}" class="btn btn-sm btn-danger" title="Удалить из корзины"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<a href="/cart/checkout" class="btn btn-success">Следующий шаг <i class="fa fa-long-arrow-right"></i></a>
									<a href="/cart/destroy" class="btn btn-danger">Очистить корзину</a>
								</td>
								<td colspan="2">
									<strong>Итого: {!! Cart::subtotal() !!} грн</strong>
								</td>
							</tr>
						</tfoot>
					</table>
				</article>
			</div>
		</div>
	</section>
@stop

@section('scripts')
	<script src="/build/js/cart.js"></script>
@stop