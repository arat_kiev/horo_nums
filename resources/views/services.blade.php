<section id="services" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2>{!! $data['pages']['services']['title'] !!}</h2>
                    {!! $data['pages']['services']['body'] !!}
                </div>

                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <h4 class="page-header text-center">Нумерология</h4>

                      <div class="panel-group" id="accordion-numeric" role="tablist" aria-multiselectable="true">
                        @if(!empty($numerics))
                          @foreach($numerics as $numeric)
                            <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="numeric-{!! $numeric['root']->id !!}">
                                <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion-numeric" href="#numeric-service-{!! $numeric['root']->id !!}" aria-expanded="false" aria-controls="numeric-service-{!! $numeric['root']->id !!}">
                                    {!! $numeric['root']->name !!}
                                  </a>
                                </h4>
                              </div>
                              <div id="numeric-service-{!! $numeric['root']->id !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="numeric-{!! $numeric['root']->id !!}">
                                <div class="panel-body">
                                    <ul class="subservices">
                                    @foreach($numeric['sub_services'] as $subservice)
                                        <li>
                                            <a href="/service/{{ $subservice->slug }}">{{ $subservice->name }}</a>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                    </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                    <h4 class="page-header text-center">Астрология</h4>

                    <div class="panel-group" id="accordion-horo" role="tablist" aria-multiselectable="true">
                      @if(!empty($horos))
                        @foreach($horos as $horo)
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="horo-{!! $horo['root']->id !!}">
                              <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-horo" href="#horo-service-{!! $horo['root']->id !!}" aria-expanded="false" aria-controls="horo-service-{!! $horo['root']->id !!}">
                                  {!! $horo['root']->name !!}
                                </a>
                              </h4>
                            </div>
                            <div id="horo-service-{!! $horo['root']->id !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="horo-{!! $horo['root']->id !!}">
                              <div class="panel-body">
                                  <ul class="subservices">
                                  @foreach($horo['sub_services'] as $subservice)
                                      <li>
                                          <a href="/service/{{ $subservice->slug }}">{{ $subservice->name }}</a>
                                      </li>
                                  @endforeach
                                  </ul>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                    </div>
                  </div>
                </div>


            </div>
        </div>
    </div>
</section>
{{--{!! $page['body'] !!}--}}