<section id="team" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2>Наша команда</h2>

                    {{--<h4>Мы - команда профессионалов, которые успешно занимаемся своим делом и бла-бла-бла...</h4>--}}
                </div>
                
                @foreach($team as $team_staff)
                <div class="col-md-4 col-md-offset-2">
                    <div class="member_detail">
                        <div class="member_img">
                            <img src="{{ asset($team_staff->photo) }}" alt="{!! $team_staff->name !!}">
                        </div>
                        <div class="member_name">
                            <h5>{!! $team_staff->name !!}</h5>
                            @if($team_staff->staff == 'horo')
                                <p>Астролог</p>
                            @elseif($team_staff->staff == 'numbers')
                                <p>Нумеролог</p>
                            @endif
                            <div class="team-item-desc">
                                {!! $team_staff->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{--<div class="col-md-4">--}}
                    {{--<div class="member_detail">--}}
                        {{--<div class="member_img">--}}
                            {{--<img src="{{ asset('/images/team/viatliy.jpg') }}" alt="Виталий">--}}
                        {{--</div>--}}
                        {{--<div class="member_name">--}}
                            {{--<h5>Виталий</h5>--}}
                            {{--<p>Астролог</p>--}}
                            {{--<div class="team-item-desc">--}}
                                {{--<p>--}}
                                    {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, aliquam beatae cupiditate delectus doloribus enim error eum expedita magnam minus molestias nostrum obcaecati officiis provident quasi repellendus sapiente vel, vitae?--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</section>