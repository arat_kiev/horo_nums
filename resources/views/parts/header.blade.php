<header class="navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="header_top">
                <div class="col-md-2">
                    <div class="logo_img">
                        <a href="/#home" class="js-target-scroll">
                            <img src="/images/logo.png" alt="logoimage" class="header-logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="menu_bar">
                        @include('parts.menu')
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>