<nav role="navigation" class="navbar navbar-default">
	<div class="navbar-header">
		<button id="menu_slide" aria-controls="navbar" aria-expanded="false" data-toggle="collapse"
		        class="navbar-toggle collapsed" type="button">
			<span class="sr-only">Навигация</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="collapse navbar-collapse" id="navbar">
		<ul class="nav navbar-nav">
			@foreach($menus as $menu)
				<li><a href="/#{!! $menu->alias !!}" class="js-target-scroll main-menu-item" data-item-name="{!! $menu->name !!}">{!! $menu->name !!}</a></li>
			@endforeach
			@if(Auth::user())
					<li><a href="{!! url('profile') !!}">Личный кабинет</a></li>
				@else
					<li><a href="{!! url('login') !!}"><i class="fa fa-sign-in" aria-hidden="true"></i> Вход</a></li>
			@endif
			<li id="menu-cart" @if(Cart::count() == 0)class="hidden"@endif><a href="/cart" title="Корзина"><i class="fa fa-shopping-basket"></i> <span class="badge badge-menu">{!! Cart::count() !!}</span></a></li>
		</ul>
	</div>
</nav>