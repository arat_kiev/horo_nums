@extends('layouts.layout')

@section('seo')
	<meta name="description" content="{{ $service->meta_description }}">
	@if(!empty($service->meta_title))
		<title>{{ $service->meta_title }}</title>
	@else
		<title>{{ $service->name }}</title>
	@endif
@stop

@section('content')
	<section id="services" class="post_blog_bg primary-bg">
		<div class="container">
			<div class="row">
				
				<div class="col-md-8">
					<article class="blog_post">
						<h4>
							@if(!empty($service->meta_title))
								{{ $service->meta_title }}
							@else
								{{ $service->name }}
							@endif
							<small>| {!! $service->type->name !!}</small>
						</h4>
						@if(!empty($service->cover))
							<div class="blog_post_img">
								<img src="/{!! $service->cover !!}" alt="image" class="img-responsive img-thumbnail" style="min-width: 100%;">
							</div>
						@endif
						<div class="text-justify">
							{!! $service->body !!}
						</div>
					</article>
				</div>
				
				<div class="col-md-4">
					<aside>
						<div class="side_blog_bg">
							<div class="sidebar_wrap">
								<div class="side_bar_heading">
									<h6>Заказать</h6>
									<h5>
										@if($service->old_price > 0)
											<del>{!! $service->old_price !!}</del>&nbsp;
										@endif
										{!! $service->price !!} грн
									</h5>
									<button type="button" class="btn btn-block btn-warning" id="add-to-cart" data-service-id="{{ $service->id }}"><i class="fa fa-cart-arrow-down"></i> Заказать</button>
								</div>
							</div>
						</div>
					</aside>
					
					@if(count($other_services) > 0)
					<aside>
						<div class="side_blog_bg other-services-block">
							
							<div class="sidebar_wrap">
								<div class="side_bar_heading">
									<h6>Другие услуги</h6>
								</div>
								
								@foreach($other_services as $other_service)
								<div class="recent-detail">
									@if(!empty($other_service->cover))
									<div class="recent-image">
										<a href="/service/{!! $other_service->slug !!}">
											<img src="/{!! $other_service->cover !!}" alt="{!! $other_service->name !!}">
										</a>
									</div>
									@endif
									<div class="recent-text">
										<h6>
											<a href="/service/{!! $other_service->slug !!}">
												{!! $other_service->name !!}
											</a>
										</h6>
										<div class="blog_category side_category">
											<ul>
												<li>
													{!! $other_service->type->name !!}
												</li>
											</ul>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</aside>
					@endif
				</div>
			</div>
		</div>
	</section>
@stop

@section('scripts')
	<script src="/build/js/cart.js"></script>
@stop