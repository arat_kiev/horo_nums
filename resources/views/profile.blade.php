@extends('layouts.layout')

@section('seo')
	<meta name="description" content="">
	<title>Личный кабинет |  {!! $user->name !!}</title>
@stop

@section('styles')
	{{--<link rel="stylesheet" href="{{ asset('js/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">--}}
	<link rel="stylesheet" href="{{ asset('js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('content')
	<section class="post_blog_bg primary-bg">
		<div class="container">
			<div class="row">
				<article class="blog_post" style="padding-top: 40px">
					<div class="col-md-4">
						<div class="page-header">
							<h4>Профиль</h4>
						</div>
						@if (session('status1'))
							<div class="alert alert-success">
								{{ session('status1') }}
							</div>
						@endif
						{!! Form::model($user, ['url' => 'profile/update', 'method' => 'post']) !!}
						<div class="form-group">
							<label>Фамилия *</label>
							{!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Фамилия', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							<label>Имя *</label>
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							<label>Отчество *</label>
						{!! Form::text('middle_name', null, ['class' => 'form-control', 'placeholder' => 'Отчество', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							Мужчина
							@if($user->sex == 'male')
							{!! Form::radio('sex', 'male', true) !!}
							@else
								{!! Form::radio('sex', 'male') !!}
							@endif
							Женщина
							@if($user->sex == 'female')
								{!! Form::radio('sex', 'female', true) !!}
							@else
								{!! Form::radio('sex', 'female') !!}
							@endif
						</div>
						<div class="form-group">
							<label>Дата рождения *</label>
							{!! Form::date('birth_date', null, ['class' => 'form-control', 'id' => 'userBirthDate', 'placeholder' => 'Дата рождения', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							<label>Время рождения *</label>
							{!! Form::time('birth_time', null, ['class' => 'form-control', 'placeholder' => 'Время рождения', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							<label>Место рождения *</label>
							<div class="help-block">Формат ввода: Страна, населенный пункт. Например: Украина, Киев</div>
							{!! Form::text('birth_place', null, ['class' => 'form-control', 'placeholder' => 'Место рождения', 'required' => 'required']) !!}
						</div>
						<div class="form-group">
							<label>Email *</label>
							{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
						</div>
						{!! Form::hidden('id') !!}
						{!! Form::macro('profileSubmit', function()
								{
								    return '<div class="form-group"><button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Сохранить</button><a href="/logout" class="btn btn-danger" style="margin-left: 15px;"><i class="fa fa-sign-out"></i> Выйти</a></div>';
								})
						!!}
						{!! Form::profileSubmit() !!}
						{!! Form::close() !!}
					</div>
					<div class="col-md-8">
						<div class="page-header">
						  <h4>Мои заказы</h4>
						</div>
						@if(!empty($orders) && count($orders) > 0)
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Дата заказа</th>
										<th>Счет</th>
										<th>Статус оплаты</th>
										<th>Статус заказа</th>
										<th>Результат</th>
									</tr>
								</thead>
								<tbody>
								@foreach($orders as $order)
									<tr>
										<td>{!! \Carbon\Carbon::parse($order->created_at)->format('d.m.Y') !!}</td>
										<td>{!! $order->invoice !!}</td>
										<td>
											@if($order->payed == true)
												<span class="label label-success">Оплачен</span>
												@else
												<span class="label label-danger">Не оплачен</span>
											@endif
										</td>
										<td>
											<span class="label" style="background-color: {!! $order->status->color !!}">
												{!! $order->status->name !!}
											</span>
										</td>
										<td>
											@if(!empty($order->result))
													<a href="{!! $order->result !!}" @if($order->payed !== true)class="pay-order" data-order-id="{!! $order->id !!}"@endif>Получить заказ</a>
												@elseif($order->payed == false)
													<a href="{!! url('/cart/payment/'.$order->id) !!}">Оплатить</a>
												@else
													В обработке
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							@else
							<div class="alert alert-info">
								<i class="fa fa-info"></i> Вы еще не воспользовались нашими услугами. Выберите <a href="{!! url('/#services') !!}">услугу</a>
								и мы с радость в кратчайшие сроки выполним Ваш заказ!
							</div>
						@endif
					</div>
				</article>
			</div>
		</div>
	</section>
@stop

@section('scripts')
	{{--<script src="{{ asset('js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>--}}
	{{--<script src="{{ asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js') }}"></script>--}}
	<script src="{{ asset('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.ru.js') }}"></script>
	<script src="{{ asset('build/js/profile.js') }}"></script>
@stop