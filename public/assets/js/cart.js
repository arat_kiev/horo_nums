//Обработка получения сервиса в корзину
$('.add-to-cart, #add-to-cart').click(function(event) {
  var setId = $(this).data('set-id');

  $.ajax({
    dataType: 'json',
    url: '/cart/add',
    type: 'post',
    data: {
      service_id: $(this).data('service-id'),
      set_id: $(this).data('set-id')
    },
    success: function(result) {
      $('#menu-cart').removeClass('hidden');
      $('.badge.badge-menu').html(result);
      
      $('#add-to-cart').removeClass('btn-warning')
          .addClass('disabled')
          .addClass('btn-success')
          .html('<i class="fa fa-check "></i> В корзине')
          .attr({'disabled': 'disabled'});
      
      $('#pricing-' + setId).removeClass('btn-warning')
          .addClass('disabled')
          .addClass('btn-success')
          .html('<i class="fa fa-check "></i> В корзине')
          .attr({'disabled': 'disabled'});
    },
    error: function(result) {
      console.log(result);
    }
  });
  
});
